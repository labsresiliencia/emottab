# EmoTab

El EmoTab es un dispositivo electrónico que
cuenta se coloca en las comunidades para
contabilizar el estado de ánimo de los habitantes
a través de botones que representan emociones.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"EmoTab" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: Karla Hernández @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Buzzer                          |        1 |
| Boton táctil                    |        1 |
| Resistencia de 330Ohm           |        3 |
| LED Rojo                        |        1 |
| LED Naranja                     |        1 |
| LED Verde                       |        1 |
| Breadboard                      |        1 |
| Cable Jumper                    |       16 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino.

| Componente                         |        |         |
|------------------------------------|--------|---------|
| Buzzer                             | BB-J1  | BB-J2   |
| Jumper 1                           | BB-I2  | AR-10   |
| LED Rojo                           | BB-I8  | BB-I9   |
| LED Anaranjado                     | BB-I11 | BB-I12  |
| LED Verde                          | BB-I14 | BB-I15  |
| Resistencia 1                      | BB-F8  | BB-D8   |
| Resistencia 2                      | BB-F11 | BB-D11  |
| Resistencia 3                      | BB-F14 | BB-D14  |
| Jumper 2                           | BB-I9  | AR-9    |
| Jumper 3                           | BB-I12 | AR-8    |
| Jumper 4                           | BB-I15 | AR-7    |
| Boton 1                            | BB-F20 | BB-F18  |
| Boton 2                            | BB-F25 | BB-F23  |
| Boton 3                            | BB-F30 | BB-F28  |
| Jumper 5                           | BB-I20 | AR-4    |
| Jumper 6                           | BB-I25 | AR-3    |
| Jumper 7                           | BB-I30 | AR-5    |
| Jumper 8                           | BB-F1  | BB-V-   |
| Jumper 9                           | BB-A9  | BB-V-   |
| Jumper 10                          | BB-A11 | BB-V-   |
| Jumper 11                          | BB-A14 | BB-V-   |
| Jumper 12                          | BB-A18 | BB-V-   |
| Jumper 13                          | BB-A23 | BB-V-   |
| Jumper 14                          | BB-A28 | BB-V-   |
| Jumper 15 (Conecta de lado a lado) | BB-V-  | BB-V-   |
| Jumper 16 (Conecta de lado a lado) | BB-V+  | BB-V-   |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_v5***.
