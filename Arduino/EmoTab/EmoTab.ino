int lmolesto = 2;
int lmeh = 3;
int lfeliz = 4;
int bmolesto = 8;
int bmeh = 9;
int bfeliz = 10;
int soloSueno = 6;

enum estado_sistema {
  es_espera, es_evaluar,

};

void setup() {
  pinMode(lmolesto, OUTPUT);
  pinMode(lmeh,     OUTPUT);
  pinMode(lfeliz,   OUTPUT);
  pinMode(bmolesto, INPUT_PULLUP);
  pinMode(bmeh,     INPUT_PULLUP);
  pinMode(bfeliz,   INPUT_PULLUP);
  Serial.begin(9600);

}

void loop() {

  automata_finito();
  //prueba_bocina();
  //prueba_led();
}

void automata_finito() {
  static estado_sistema estado = es_espera;
  static long t_inicio;

  long t_actual = millis();
  switch (estado) {
    case es_espera:
      if (!digitalRead(bmolesto)) {
        digitalWrite(lmolesto, HIGH);
        tone(soloSueno, 800, 500);
        t_inicio = t_actual;
        estado = es_evaluar;
        Serial.println("Me siento molesto >:(");
      }
      if (!digitalRead(bmeh)) {
        digitalWrite(lmeh, HIGH);
        tone(soloSueno, 1000, 500);
        t_inicio = t_actual;
        estado = es_evaluar;
        Serial.println("Me siento ni feliz, ni molesto... estoy meh -_-");
      }
      if (!digitalRead(bfeliz)) {
        digitalWrite(lfeliz, HIGH);
        tone(soloSueno, 1200, 500);
        t_inicio = t_actual;
        estado = es_evaluar;
        Serial.println("Me siento feliz :)");
      }
      break;
    case es_evaluar:

      if (t_actual - t_inicio >= 1000) {
        digitalWrite(lmolesto, LOW);
        digitalWrite(lmeh, LOW);
        digitalWrite(lfeliz, LOW);
        estado = es_espera;
      }

      break;
  }
}

void prueba_bocina() {

  int tiempol = 1000;

  analogWrite(soloSueno, 10);
  delay(tiempol);                 // Espera
  analogWrite(soloSueno, 0);      // Apaga
  delay(tiempol);                 // Espera
}

void prueba_led() {
  int tiempol = 100;

  digitalWrite(lmolesto, HIGH);
  delay(tiempol);
  digitalWrite(lmolesto, LOW);
  delay(tiempol);
  digitalWrite(lmeh, HIGH);
  delay(tiempol);
  digitalWrite(lmeh, LOW);
  delay(tiempol);
  digitalWrite(lfeliz, HIGH);
  delay(tiempol);
  digitalWrite(lfeliz, LOW);
  delay(tiempol);
}
